This is a Client for 'my-trip' application.

To run app:

    yarn install && yarn dev
    
To run unit tests:

    yarn test
    
Used technologies:
- React
- Redux
- Typescript
- Webpack
- Postcss