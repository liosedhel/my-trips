# Configure the AWS Provider
provider "aws" {
  region     = "${var.region}"
}

terraform {
  backend "s3" {
    bucket = "marbor-terraform-state-dev"
    key    = "dev/my_trip/terraform.tfstate"
    region = "eu-central-1"
  }
}

module "create_cloud_front_distribution" {
  source = "../../../modules/cloudfront_distribution"
  bucket_name = "dev-my-trip"
  environment = "dev"
  origin_access_identity_id = "E2RU72CBNA4064"
  cloudfront_comment = "This is a My trip distribution"
  cloudfront_logging_bucket = "mytrip-logs.s3.amazonaws.com"
  cloudfront_logging_prefix = "my-trip"
  s3_origin_id = "myTripOriginId"
}