resource "aws_s3_bucket" "bucket" {
  bucket = "${var.bucket_name}"
  acl    = "${var.acl}"
  force_destroy = true

  tags {
    Name        = "${var.bucket_name}"
    Environment = "${var.environment}"
  }
}

locals {
  s3_origin_id = "${var.s3_origin_id}"
}

resource "aws_cloudfront_distribution" "s3_distribution" {
  origin {
    domain_name = "${aws_s3_bucket.bucket.bucket_regional_domain_name}"
    origin_id   = "${local.s3_origin_id}"

    s3_origin_config {
      origin_access_identity = "origin-access-identity/cloudfront/${var.origin_access_identity_id}"
    }
  }

  enabled             = "${var.cloudfront_enabled}"
  is_ipv6_enabled     = "${var.is_ipv6_enabled}"
  comment             = "${var.cloudfront_comment}"
  default_root_object = "${var.default_root_object}"

  logging_config {
    include_cookies = "${var.include_cookies}"
    bucket          = "${var.cloudfront_logging_bucket}"
    prefix          = "${var.cloudfront_logging_prefix}"
  }

 # aliases = ["mysite.example.com", "yoursite.example.com"]

  default_cache_behavior {
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "${local.s3_origin_id}"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
    viewer_protocol_policy = "redirect-to-https"
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  tags {
    Environment = "${var.environment}"
  }

  viewer_certificate {
    cloudfront_default_certificate = true
  }
}