variable "bucket_name" {
  description = "S3 unique bucket name."
}
variable "acl" {
  default = "private"
}
variable "environment" {
}

variable "origin_access_identity_id" {}

variable "cloudfront_comment" {
  default = ""
}

variable "default_root_object" {
  default = "index.html"
}

variable "cloudfront_logging_bucket" {}
variable "cloudfront_logging_prefix" {}

variable "s3_origin_id" {}
variable "cloudfront_enabled" {
  default = true
}
variable "is_ipv6_enabled" {
  default = true
}
variable "include_cookies" {
  default = false
}