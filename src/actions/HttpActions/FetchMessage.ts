import {FETCH_MESSAGE} from "./types";
import {AUTH_SERVER_URL} from '../../config'
import axios from 'axios'
import {Dispatch} from 'redux';
import State from "../../reducers/State";

export function fetchMessage() {
    return function (dispatch: Dispatch<any>) {
        axios.get(AUTH_SERVER_URL, {
            headers: {authorization: localStorage.getItem('token')}
        })
        .then((response) => {
            dispatch({
                type: FETCH_MESSAGE,
                payload: response.data.message
            });
        })
        .catch(({response}) => {

        });
    }
}