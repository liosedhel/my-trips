import Action from "../Action";
import Place from "../../domain/Place";
import {REMOVE_PLACE} from "./types";

export function removePlace(place :Place): Action<Place> {
    return {
        type: REMOVE_PLACE,
        payload: place
    };
}