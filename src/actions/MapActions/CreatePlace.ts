import Action from "../Action";
import Place from "../../domain/Place";
import {CREATE_PLACE} from "./types";

export function createPlace(place :Place): Action<Place> {
    return {
        type: CREATE_PLACE,
        payload: place
    };
}