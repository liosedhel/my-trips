import Action from "../Action";
import Place from "../../domain/Place";
import {UPDATE_PLACE} from "./types";

export function updatePlace(place :Place): Action<Place> {
    return {
        type: UPDATE_PLACE,
        payload: place
    };
}