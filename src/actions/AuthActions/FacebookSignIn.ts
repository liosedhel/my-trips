import {AUTH_USER} from './types';
import history from '../../history'
import {FACEBOOK_TOKEN} from '../../global/localStorageKeys';
import {Dispatch} from 'redux';
import State from '../../reducers/State';

export function facebookSignIn(name: string, userId: string, acessToken: string) {
    return function (dispatch: Dispatch<any>) {
        dispatch({type: AUTH_USER});
        localStorage.setItem(FACEBOOK_TOKEN, acessToken);
        history.push('app');
    }
}