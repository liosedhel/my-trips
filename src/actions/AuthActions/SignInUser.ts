import {AUTH_USER} from "./types";
import {authError} from "./AuthError";
import {AUTH_SERVER_URL} from '../../config'
import axios from 'axios'
import history from '../../history'
import {Dispatch} from 'redux'
import State from "../../reducers/State";
import {AUTH_SERVER_TOKEN} from "../../global/localStorageKeys";

export function signInUser({email, password}) {
    return function (dispatch: Dispatch<any>) {
        axios.post(`${AUTH_SERVER_URL}/signin`, {email, password})
            .then(response => {
                dispatch({type: AUTH_USER});
                localStorage.setItem(AUTH_SERVER_TOKEN, response.data.token);
                history.push('app');
            })
            .catch(() => {
                dispatch(authError('Bad login info'));
            });
    }
}