import {SIGNUP_CAPTCHA_SOLVED} from "./types";

export function signUpCaptchaSolved() {
    return {type: SIGNUP_CAPTCHA_SOLVED}
}