import {AUTH_USER} from "./types";
import {authError} from "./AuthError";
import {AUTH_SERVER_URL} from '../../config'
import {Dispatch} from 'redux';
import axios from 'axios'
import history from '../../history'
import State from "../../reducers/State";


export function signUpUser({email, password}) {
    return function (dispatch: Dispatch<any>) {
        axios.post(`${AUTH_SERVER_URL}/signup`, {email, password})
            .then(response => {
                dispatch({type: AUTH_USER});
                localStorage.setItem('token', response.data.token);
                history.push('app');
            })
            .catch(({response}) => {
                dispatch(authError(response.data.error));
            });
    }
}
