import {UNAUTH_USER} from "./types";
import history from '../../history'
import {FACEBOOK_TOKEN, AUTH_SERVER_TOKEN, GOOGLE_TOKEN} from "../../global/localStorageKeys";

export function signOutUser() {
    localStorage.removeItem(FACEBOOK_TOKEN);
    localStorage.removeItem(AUTH_SERVER_TOKEN);
    localStorage.removeItem(GOOGLE_TOKEN);

    history.push('/');
    return {type: UNAUTH_USER}
}