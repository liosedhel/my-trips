import {AUTH_USER} from './types';
import history from '../../history'
import {GOOGLE_TOKEN} from '../../global/localStorageKeys';
import {Dispatch} from 'redux';
import State from '../../reducers/State';

export function googleSignIn(name: string, userId: string, accessToken: string) {
    return function (dispatch: Dispatch<any>) {
        dispatch({type: AUTH_USER});
        localStorage.setItem(GOOGLE_TOKEN, accessToken);
        history.push('app');
    }
}