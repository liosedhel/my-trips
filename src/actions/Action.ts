export default interface Action<P> {
    type: string;
    payload: P;
    error?: boolean;
    meta?: object;
}