export default interface Place {
    longitude: number
    latitude: number
    name?: string
    gallery?: string
}