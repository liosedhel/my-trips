import Place from "./Place";

const places: Place[] = [
    {
        longitude: 19.9449799,
        latitude: 50.0646501,
        name: 'Cracow',
        gallery: 'https://link-to-gallery.google.pl'
    },
    {
        longitude: 21.017532,
        latitude: 52.237049,
        name: 'Warsaw',
        gallery: 'https://link-to-gallery.google.pl'
    }
];

export default places;