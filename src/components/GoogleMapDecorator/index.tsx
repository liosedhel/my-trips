import * as React from 'react'
import {withGoogleMap, withScriptjs} from "react-google-maps"
import {compose, withProps} from 'recompose'
import Map from "../../containers/Map/Map";
import {GOOGLE_API_KEY} from "../../config";

export const MapComponent: React.ComponentClass<any> =
    compose<any, any>(
        withProps({
            googleMapURL: `https://maps.googleapis.com/maps/api/js?key=${GOOGLE_API_KEY}&v=3.exp&libraries=geometry,drawing,places`,
            loadingElement: <div style={{height: `100%`}}/>,
            containerElement: <div style={{height: `95%`}}/>,
            mapElement: <div style={{height: `100%`}}/>,
        }),
        withScriptjs,
        withGoogleMap
    )((props) =>
        <Map/>
    );

export default MapComponent;