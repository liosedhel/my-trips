import * as React from 'react';
import {shallow} from 'enzyme';
import PlaceMarker, {PlaceMarkerProps, PlaceMarkerState} from './';
import places from '../../domain/MockPlacesData'
import {default as Marker, MarkerProps} from "react-google-maps/lib/components/Marker";

describe("PlaceMarker", () => {
    it('Initial state should be correct', () => {
        const popUp = shallow<PlaceMarkerProps>(<PlaceMarker place={places[0]}/>);

        expect(popUp.state())
            .toEqual({isOpen: false});
        expect(popUp.find('PlaceMarkerPopUp').length)
            .toBe(0);
    });

    it('Marker should have correct coordinates', () => {
        const popUp = shallow<PlaceMarkerProps>(<PlaceMarker place={places[0]}/>);

        expect(popUp.find<MarkerProps>(Marker).props().position.lat)
            .toEqual(places[0].latitude);
        expect(popUp.find<MarkerProps>(Marker).props().position.lng)
            .toEqual(places[0].longitude);
    });

    it('On Marker click state should change', () => {
        const popUp = shallow<PlaceMarkerProps, PlaceMarkerState>(
            <PlaceMarker place={places[0]}/>
        );

        popUp.find('Marker').simulate('click');

        expect(popUp.state())
            .toEqual({isOpen: true});
        expect(popUp.find('MarkerPlacePopUp').length)
            .toEqual(1);
    });
});
