import * as React from 'react'
import {Marker} from 'react-google-maps'
import MarkerTripPopUp from '../../containers/PlaceMarkerPopUp';
import Place from "../../domain/Place";


export interface PlaceMarkerProps {
    place: Place
}

export interface PlaceMarkerState {
    isOpen: boolean;
}

export default class PlaceMarker extends React.Component<PlaceMarkerProps, PlaceMarkerState> {
    constructor(props) {
        super(props);
        this.state = {isOpen: false};
    }

    onToggleOpen = () => {
        this.setState((state) => {
            return {isOpen: !state.isOpen}
        })
    };
    onMarkerTripPopUpClose = () => {
        this.setState({isOpen: false});
    };

    render() {
        return (
            <div>
                <Marker
                    position={{lat: this.props.place.latitude, lng: this.props.place.longitude}}
                    onClick={this.onToggleOpen}
                >
                    {this.state.isOpen && <MarkerTripPopUp onCloseClick={this.onMarkerTripPopUpClose} place={this.props.place}/>}
                </Marker>
            </div>
        )
    }
}