import * as React from 'react'
import Map from '../GoogleMapDecorator';
import * as styles from './style.css'

class MapArea extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div className={styles.fullHeight}>
                <Map />
            </div>

        );
    }
}

export default MapArea;