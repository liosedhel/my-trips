import * as React from 'react'
import { Route } from 'react-router-dom'
import * as style from './style.css';
import Header from '../../containers/Header'
import MapArea from '../MapArea/index'
import Welcome from '../Welcome'
import SignOut from '../../containers/SignOut'
import RequireAuth from '../../containers/RequireAuth'
import Start from '../Start';

export namespace App {
    export interface Props {}
    export interface State {}
}

export default class App extends React.Component<App.Props, App.State> {
    render() {
        return (
            <div className={style.mainBody}>
                <Header />
                <Route exact path='/' component={Welcome}/>
                <Route path='/start' component={Start}/>
                <Route path='/signout' component={SignOut}/>
                <Route path='/app' component={RequireAuth(MapArea)}/>
            </div>
        );
    }
}
