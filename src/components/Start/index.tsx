import * as React from 'react'
import {Component} from 'react';
import FacebookSignIn from '../../containers/FacebookSignIn'
import GoogleSignIn from '../../containers/GoogleSignIn'
import * as globalTextStyle from '../../global/styles/text.css'

class Start extends Component {
    render() {
        return (
            <div className='container'>
                <div className='row s4 '>
                    <h4 className={globalTextStyle.mainFont}>Continue with:</h4>
                </div>
                <div className='row'>
                    <div className='col s2 m2 l1'>
                        <FacebookSignIn/>
                    </div>
                    <div className='col s12 m4 l2'>
                        <GoogleSignIn/>
                    </div>
                </div>
            </div>
        );
    }
}

export default Start;