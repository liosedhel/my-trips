import * as React from 'react'
import {Component} from "react";
import history from '../../history'
import * as globalStyles from '../../global/styles/button.css'
import * as globalTextStyle from '../../global/styles/text.css'

class Welcome extends Component {

    render() {
        const buttonStyle= `btn col s12 m4 l2 ${globalStyles.mainButton}`;

        return (
            <div className='container margins'>
                <div className='col s12 m4 l2'>
                    <h4 className={globalTextStyle.mainFont}>
                        Create Your Trip Map!
                    </h4>
                    <div className={buttonStyle} onClick={() => history.push('start')}>
                        Get started
                    </div>
                </div>
            </div>
        );
    }
}

export default Welcome;