import AuthState from './AuthReducer/AuthState'
import PlacesState from './PlacesReducer/PlacesState'
import { FormStateMap } from "redux-form/lib/reducer";

//TODO not sure about form type
export default interface ReduxState {
    form: FormStateMap;
    auth: AuthState;
    places: PlacesState
}