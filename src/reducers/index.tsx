import { combineReducers } from 'redux';
import {reducer as formReducer} from 'redux-form'
import authReducer from './AuthReducer'
import PlaceReducer from './PlacesReducer'
import State from './State'

const rootReducer = combineReducers<State>({
    form: formReducer,
    auth: authReducer,
    places: PlaceReducer
});

export default rootReducer;
