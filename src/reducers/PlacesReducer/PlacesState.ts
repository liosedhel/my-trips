import Place from "../../domain/Place";

type PlaceState = Place[];

export default PlaceState;