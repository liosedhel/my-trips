import { CREATE_PLACE, REMOVE_PLACE, UPDATE_PLACE } from '../../actions/MapActions/index'
import Action from "../../actions/Action";
import Place from "../../domain/Place";
import PlaceState from "./PlacesState";

function matches(place: Place, givenPlace: Place) {
    return place.latitude === givenPlace.latitude && place.longitude === givenPlace.longitude;
}

export default (state: PlaceState = [], action: Action<Place>): PlaceState => {
    switch (action.type) {
        case CREATE_PLACE:
            return [
                ...state,
                action.payload
            ];
        case REMOVE_PLACE:
            return state.filter((place: Place) => !matches(place, action.payload));
        case UPDATE_PLACE:
            const newState = state.filter((place : Place) => !matches(place, action.payload));
            newState.push(action.payload);
            return newState;
        default:
            return state;
    }
}
