import {AUTH_ERROR, AUTH_USER, FETCH_MESSAGE, SIGNUP_CAPTCHA_SOLVED, UNAUTH_USER} from "../../actions/AuthActions";
import AuthState from './AuthState'
import Action from "../../actions/Action";

export default function(state: AuthState = {authenticated: false}, action: Action<string>): AuthState {

    switch (action.type){
        case AUTH_USER:
            return {...state, error: '', authenticated: true};
        case UNAUTH_USER:
            return {...state, authenticated: false};
        case AUTH_ERROR:
            return {...state, error: action.payload};
        case FETCH_MESSAGE:
            return {...state, message: action.payload};
        case SIGNUP_CAPTCHA_SOLVED:
            return{...state, isSignUpCaptchaSolved: true};
    }

    return state;
}