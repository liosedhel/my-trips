export default interface AuthState {
    authenticated: boolean;
    error?: string;
    message?: string;
    isSignUpCaptchaSolved?: boolean;
}