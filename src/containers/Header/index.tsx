import * as React from 'react'
import {connect} from 'react-redux'
import {Component} from 'react';
import {Link} from "react-router-dom";
import * as styles from './styles.css'
import * as globalStyle from '../../global/styles/button.css'

interface HeaderProps {
    authenticated?: boolean;
}

class Header extends Component<HeaderProps> {
    renderLinks = () => {
        if (this.props.authenticated) {
            return (
                <li>
                    <Link to='signout'>Sign Out</Link>
                </li>
            );
        }
        else {
            return [
                <li key={0}>
                    <Link to='/start'>Create Your Map</Link>
                </li>
            ]
        }

    };

    renderBrand() {
        const logoClasses = `${styles.logoStyle}`;
        if (this.props.authenticated) {
            return <Link to='/app' className={logoClasses}>My Trip</Link>;
        }
        else {
            return <Link to='/' className={logoClasses}>My Trip</Link>;
        }
    }

    render() {
        const navClasses = `${styles.navbarColor} nav-wrapper`;
        return (
            <nav>
                <div className={navClasses}>
                    {this.renderBrand()}
                    <a href="#" data-activates="mobile-demo" className="button-collapse">
                        <i className="material-icons">menu</i>
                    </a>
                    <ul className='right hide-on-med-and-down'>
                        {this.renderLinks()}
                    </ul>
                    <ul className="side-nav" id="mobile-demo">
                        {this.renderLinks()}
                    </ul>
                </div>
            </nav>
        );
    }
}

function mapStateToProps(state) {
    return {authenticated: state.auth.authenticated}
}

export default connect(mapStateToProps, null)(Header);