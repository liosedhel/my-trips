import * as React from 'react';
import {Component} from 'react';
import { connect } from 'react-redux';
import {RouteComponentProps, RouterChildContext, withRouter} from "react-router";
import * as PropTypes from 'prop-types'

interface RequireAuthProps extends RouteComponentProps<any>{
    authenticated: boolean
}

export default function(ComposedComponent) {
    class Authentication extends Component<RequireAuthProps> {
        static contextTypes = {
            router: PropTypes.object
        };

        componentWillMount() {
            if (!this.props.authenticated) {
                this.props.history.push('/');
            }
        }

        componentWillUpdate(nextProps) {
            if (!nextProps.authenticated) {
                this.context.router.push('/');
            }
        }

        render() {
            return <ComposedComponent {...this.props} />
        }
    }

    function mapStateToProps(state) {
        return { authenticated: state.auth.authenticated };
    }

    return connect(mapStateToProps)(withRouter(Authentication));
}
