import TripMarker from '../../components/PlaceMarker/index'
import * as React from 'react'
import { GoogleMap } from "react-google-maps"
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import Place from "../../domain/Place";
import { createPlace } from '../../actions/MapActions'
import Action from "../../actions/Action";

export interface MapProps {
    places?: Place[];
    createPlace?: (place: Place) => Action<Place>
}

class Map extends React.Component<MapProps> {
    constructor(props) {
        super(props);
    }

    onClick = (event: google.maps.MouseEvent | google.maps.IconMouseEvent) => {
        this.props.createPlace({
            longitude: event.latLng.lng(),
            latitude: event.latLng.lat()
        });
    };

    createMarkers = () => {
        return this.props.places.map(place =>
            <TripMarker
                place={place}
                key={place.longitude + place.longitude}
            />
        )
    };

    render() {
        return (
            <GoogleMap
                defaultZoom={3}
                defaultCenter={{lat: 50.0646501, lng: 19.9449799}}
                onClick={this.onClick}
            >
                {
                    this.createMarkers()
                }
            </GoogleMap>
        )
    }
}

function mapStateToProps(state) {
    return {places: state.places === undefined ? [] : state.places}
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({createPlace: createPlace}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Map);