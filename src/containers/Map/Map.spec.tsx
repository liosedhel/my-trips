import * as React from 'react'
import Map, {MapProps} from './Map'
import {GoogleMap} from "react-google-maps"
import * as Enzyme from 'enzyme'
import {shallow} from 'enzyme'
import {createMockStore} from 'redux-test-utils'
import places from '../../domain/MockPlacesData'
import {CREATE_PLACE, createPlace} from "../../actions/MapActions";

const googleMapEvent = {
    latLng: {
        lng: (): number => places[0].longitude,
        lat: (): number => places[0].latitude
    }
};

describe('Map Tests', () => {
    it('Initial state should be correct', () => {
        const testState = {
            places: places
        };
        const store = createMockStore(testState);

        const map = shallow<MapProps>(<Map/>, {context: {store}});

        expect(map.props().places)
            .toEqual(places);
        expect(map.dive().find('PlaceMarker').length)
            .toEqual(2);
    });

    it('Click in map and add place', () => {
        const testState = {
            places: []
        };
        const store = createMockStore(testState);

        const map = Enzyme.shallow<MapProps>(<Map/>, {context: {store}});

        map.dive().find('GoogleMap')
            .simulate('click', googleMapEvent);

        expect(store.isActionDispatched({
                type: CREATE_PLACE,
                payload: {
                    longitude: places[0].longitude,
                    latitude: places[0].latitude
                }
            }
        )).toBe(true);

        /*expect(map.props().places[0].latitude)
            .toEqual(places[0].latitude);
        expect(map.props().places[0].longitude)
            .toEqual(places[0].longitude);
        expect(map.dive().find('PlaceMarker').length)
            .toEqual(1);*/
    });
});