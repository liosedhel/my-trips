import * as React from 'react'
import {Component} from 'react';
import {connect} from 'react-redux'
import * as actions from '../../actions/AuthActions'
import Action from "../../actions/Action";
import { bindActionCreators, Dispatch } from "redux";
import State from "../../reducers/State";
import {signInUser, signOutUser} from "../../actions/AuthActions";

interface SignOutProps {
    signOutUser: () => Action<{}>
}
class SignOut extends Component<SignOutProps> {
    componentWillMount() {
        this.props.signOutUser();
    }
    render() {
        return (
            <div>
                See you soon...
            </div>
        );
    }
}

function mapDispatchToProps(dispatch: Dispatch<any>){
    return bindActionCreators({signOutUser: signOutUser}, dispatch)
}

export default connect<any>(null, mapDispatchToProps)(SignOut);