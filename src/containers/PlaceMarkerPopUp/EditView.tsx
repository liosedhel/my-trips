import * as React from 'react';
import { reduxForm, Field, InjectedFormProps } from 'redux-form'
import { connect } from 'react-redux'
import { requiredValidator, emailValidator, minLengthValidator } from '../../global/reduxFormValidator'
import { bindActionCreators } from "redux";
import { removePlace, updatePlace } from '../../actions/MapActions/';
import Place from '../../domain/Place';
import Action from '../../actions/Action';
import * as styles from './styles.css'
import * as globalStyles from "../../global/styles/button.css";

//TODO remove input margin
const renderInput = field => (
    <div>
        <input className={styles.zeroMargin} {...field.input} type={field.type} placeholder={field.placeholder}/>
        {field.meta.touched && field.meta.error &&
        <span className={`${styles.error}`}>{field.meta.error}</span>}
    </div>
);

interface EditViewProps {
    place: Place;
    removePlace?: (place: Place) => Action<Place>;
    updatePlace?: (place: Place) => Action<Place>;
    onSaveCallback: () => void;
}

interface EditVieState {
    name: string;
    gallery: string;
}

interface FormProps {
    name: string;
    gallery: string
}

class EditView extends React.Component<EditViewProps & InjectedFormProps, EditVieState> {
    onRemoveBtnClick = () => {
        this.props.removePlace(this.props.place);
    };

    handleSubmit = (formProps: FormProps) => {
        this.props.place.gallery = (formProps.gallery !== undefined) ? formProps.gallery : this.props.place.gallery;
        this.props.place.name = (formProps.name !== undefined) ? formProps.name : this.props.place.name;
        this.props.updatePlace(this.props.place);
        this.props.onSaveCallback();
    };

    render() {
        const buttonStyles = `btn ${globalStyles.mainButton}`;

        const {handleSubmit} = this.props;
        return (
            <div>
                <div className={styles.zeroMargin}>
                    <form onSubmit={handleSubmit(this.handleSubmit)}>
                        <Field
                            name='name'
                            component={renderInput}
                            type='text'
                            validate={this.props.place.name !== undefined ? [] : [requiredValidator]}
                            placeholder={this.props.place.name !== undefined ? this.props.place.name : 'Name'}
                        />
                        <Field
                            name='gallery'
                            component={renderInput}
                            type='text'
                            validate={this.props.place.gallery !== undefined ? [] : [requiredValidator]}
                            placeholder={this.props.place.gallery !== undefined ? this.props.place.gallery : 'Link to gallery'}
                        />
                        <br/>
                        <button type='submit' className={buttonStyles}>
                            <i className='material-icons left'>save</i>
                            Save
                        </button>
                        <button className={buttonStyles} onClick={this.onRemoveBtnClick}>
                            <i className='material-icons left'>delete_forever</i>
                            Delete
                        </button>
                    </form>
                </div>
            </div>
        )
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        removePlace: removePlace,
        updatePlace: updatePlace
    }, dispatch);
}

const EditViewFormRedux = reduxForm({
    form: 'editView'
})(EditView);

export default connect<any, any, any, any>(null, mapDispatchToProps)(EditViewFormRedux);