import * as React from 'react'
import Place from '../../domain/Place';

interface PresentingViewProps {
    place: Place;
    onEditClickCallback: () => void;
}

class PresentingView extends React.Component<PresentingViewProps> {
    openInNewTab(url) {
        const win = window.open(url, '_blank');
        win.focus();
    }

    render() {
        return (
            <div>
                <div className='row'>
                    <h6>
                        {this.props.place.name}
                    </h6>
                </div>
                <div className='row'>
                    <a className='waves-effect waves-light btn' onClick={() => this.openInNewTab(this.props.place.gallery)}>
                        <i className='material-icons left'>photo_album</i>
                        See the photos :)
                    </a>
                </div>
                <div className='row btn' onClick={this.props.onEditClickCallback}>
                    <i className='material-icons left'>edit</i>
                    Edit
                </div>
            </div>
        );
    }
}

export default PresentingView;