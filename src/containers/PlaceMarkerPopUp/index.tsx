import * as React from 'react'
import { InfoWindow } from 'react-google-maps';
import Place from '../../domain/Place';
import Action from '../../actions/Action';
import EditView from './EditView';
import PresentingView from "./PresentingView";

namespace MarkerPlacePopUp {
    export interface Props {
        onCloseClick: () => void;
        place: Place;
        removePlace?: (place: Place) => Action<Place>
    }

    export interface State {
        isEditModeEnabled: boolean
    }
}

class MarkerPlacePopUp extends React.Component<MarkerPlacePopUp.Props, MarkerPlacePopUp.State> {
    constructor(props) {
        super(props);
        this.state = {isEditModeEnabled: props.place.gallery === undefined}
    }

    onSaveCallback = () => {
        this.setState({isEditModeEnabled: false});
    };

    onEditClickCallback = () => {
        this.setState({isEditModeEnabled: true});
    };

    render() {
        return (
            <div>
                <InfoWindow onCloseClick={this.props.onCloseClick}>
                    {this.renderProperView()}
                </InfoWindow>
            </div>

        );
    }

    private renderProperView(): JSX.Element {
        if (this.state.isEditModeEnabled) {
            return <EditView place={this.props.place} onSaveCallback={this.onSaveCallback}/>
        }
        else {
            return <PresentingView place={this.props.place} onEditClickCallback={this.onEditClickCallback}/>
        }
    }
}

export default MarkerPlacePopUp;