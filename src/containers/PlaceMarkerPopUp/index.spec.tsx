import * as React from 'react';
import * as Enzyme from 'enzyme';
import {shallow} from 'enzyme';
import PlaceMarkerPopUp from './index';
import * as Styles from './style.css'
import places from '../../domain/MockPlacesData'

describe("Tests of PlaceMarkerPopUp",() => {
    it('The text should be correct', () => {
        const popUp = shallow(<PlaceMarkerPopUp onCloseClick={() => {}} place={places[0]} />);

        expect(popUp)
            .toMatchSnapshot()
    });
});
