import * as React from 'react'
import { Component } from 'react'
import { connect } from 'react-redux'
import FacebookLogin from 'react-facebook-login';
import { bindActionCreators, Dispatch } from 'redux';
import { facebookSignIn } from '../../actions/AuthActions/FacebookSignIn';
import Action from '../../actions/Action';
import { FACEBOOK_SOCIAL_LOGIN_APP_ID } from '../../config'
import * as globalStyles from '../../global/styles/button.css'
import { authError } from "../../actions/AuthActions";

interface FacebookSignInProps {
    facebookSignIn?: (name: string, userId: string, accessToken: string) => Action<null>
    authError?: (name: string) => Action<null>
}

class FacebookSignIn extends Component<FacebookSignInProps> {
    responseFacebook = (response) => {
        if (response.accessToken === undefined) {
            this.props.authError('Login with facebook unsuccessful')
        }
        else {
            this.props.facebookSignIn(response.name, response.userID, response.accessToken);
        }
    };

    render() {
        const buttonStyles = `btn ${globalStyles.mainButton}`;
        return (
            <FacebookLogin
                appId={FACEBOOK_SOCIAL_LOGIN_APP_ID}
                autoLoad={false}
                fields='name,email,picture'
                callback={this.responseFacebook}
                textButton=''
                cssClass={buttonStyles}
                icon='fa-facebook'
                disableMobileRedirect={true}
            />
        );
    }
}

function mapDispatchToProps(dispatch: Dispatch<any>) {
    return bindActionCreators(
        {
            facebookSignIn: facebookSignIn,
            authError: authError
        },
        dispatch)
}

export default connect<any>(null, mapDispatchToProps)(FacebookSignIn);