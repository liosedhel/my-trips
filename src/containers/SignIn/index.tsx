import * as React from 'react';
import {Component} from 'react';
import {reduxForm, Field, InjectedFormProps} from 'redux-form'
import {connect} from 'react-redux'
import State from '../../reducers/State'
import { bindActionCreators, Dispatch } from 'redux';
import {signInUser} from '../../actions/AuthActions';
import {requiredValidator, emailValidator} from '../../global/reduxFormValidator'
import FacebookSignIn from '../FacebookSignIn'
import GoogleSignIn from '../GoogleSignIn'
import * as styles from './styles.css'

const renderInput = field => (
    <div>
        <input className='form-control' {...field.input} type={field.type}/>
        {field.meta.touched && field.meta.error &&
        <span className={styles.error}>{field.meta.error}</span>}
    </div>
);

interface SignInProps {
    signInUser?: (input: { email: string, password: string }) => (dispatch: Dispatch<any>) => void;
    errorMessage?: string;
}

// not used currently
class SignIn extends Component<SignInProps & InjectedFormProps> {
    handleFormSubmit = ({email, password}) => {
        this.props.signInUser({email, password});
    };

    renderAlert = () => {
        if (this.props.errorMessage) {
            return (
                <div className={styles.error}>
                    <strong>Oops! </strong> {this.props.errorMessage}
                </div>
            );
        }
    };

    render() {
        const {handleSubmit} = this.props;
        return (
            <div className={styles.margins}>
                <form onSubmit={handleSubmit(this.handleFormSubmit)}>
                    <div>
                        <label htmlFor='email'>Email:</label>
                        <Field
                            name='email'
                            component={renderInput}
                            type='text'
                            validate={[requiredValidator, emailValidator]}
                        />
                    </div>
                    <div>
                        <label htmlFor="password">Password:</label>
                        <Field
                            name='password'
                            component={renderInput}
                            type='password'
                        />
                    </div>
                    {this.renderAlert()}
                    <button type='submit' className='btn btn-primary'>Sign in</button>
                </form>
                <br/>
                <FacebookSignIn />
                <br />
                <GoogleSignIn />
            </div>
        );
    }
}


function mapStateToProps(state: State) {
    return {errorMessage: state.auth.error}
}

function mapDispatchToProps(dispatch: Dispatch<any>) {
    return bindActionCreators({signInUser: signInUser}, dispatch)
}

const SignInReduxForm = reduxForm({
    form: 'signin'
})(SignIn);

export default connect<any>(mapStateToProps, mapDispatchToProps)(SignInReduxForm)