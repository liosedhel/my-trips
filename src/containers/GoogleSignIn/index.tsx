import * as React from 'react'
import {Component} from 'react';
import {connect} from 'react-redux'
import GoogleLogin from 'react-google-login';
import { bindActionCreators, Dispatch } from 'redux';
import State from '../../reducers/State';
import {googleSignIn} from '../../actions/AuthActions/GoogleSignIn';
import Action from '../../actions/Action';
import {GOOGLE_SOCIAL_LOGIN_APP_ID} from '../../config'
import * as globalStyles from "../../global/styles/button.css";
import { authError } from "../../actions/AuthActions";

interface GoogleSignInProps {
    googleSignIn?: (name: string, userId: string, accessToken: string) => Action<null>
    authError?: (name: string) => Action<null>
}

class GoogleSignIn extends Component<GoogleSignInProps> {
    response = (response) => {
        if (response.error){
            this.props.authError("Login with google unsuccessful")
        }
        else {
            this.props.googleSignIn(response.profileObj.familyName, response.googleId, response.accessToken);
        }
    };

    render() {
        const buttonStyles = `btn ${globalStyles.mainButton}`;

        return (
            <GoogleLogin
                clientId={GOOGLE_SOCIAL_LOGIN_APP_ID}
                buttonText='google'
                onSuccess={this.response}
                onFailure={(response ) => this.props.authError("Login with google unsuccessful")}
                className={buttonStyles}
                autoLoad={false}
            />
        );
    }
}

function mapDispatchToProps(dispatch: Dispatch<any>) {
    return bindActionCreators(
        {
            googleSignIn: googleSignIn,
            authError: authError
        },
        dispatch)
}

export default connect<any>(null, mapDispatchToProps)(GoogleSignIn);