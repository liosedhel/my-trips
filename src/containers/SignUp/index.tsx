import * as React from 'react';
import {reduxForm, Field, InjectedFormProps} from 'redux-form'
import {Component} from 'react';
import {connect} from 'react-redux'
import {requiredValidator, emailValidator, minLengthValidator} from '../../global/reduxFormValidator'
import { bindActionCreators, Dispatch } from "redux";
import {signUpUser, signUpCaptchaSolved} from "../../actions/AuthActions";
import Recaptcha from '../Recaptcha'
import * as styles from './styles.css'

const renderInput = field => (
    <div>
        <input className='form-control' {...field.input} type={field.type}/>
        {field.meta.touched && field.meta.error &&
        <span className={styles.error}>{field.meta.error}</span>}
    </div>
);

interface SignUpProps {
    signUpUser?: (input: { email: string, password: string }) => (dispatch: Dispatch<any>) => void;
    errorMessage?: string;
    isSignUpCaptchaSolved: boolean;
}

interface SignUpState {
    captchaErrorMessage?: string;
}

// not used currently
class SignUp extends Component<SignUpProps & InjectedFormProps, SignUpState> {
    constructor(props) {
        super(props);
        this.state = {};
    }

    handleFormSubmit = (formProps) => {
        if (this.props.isSignUpCaptchaSolved) {
            this.setState({captchaErrorMessage: null})
            this.props.signUpUser(formProps);
        }
        else {
            this.setState({captchaErrorMessage: 'Please solve the captcha'});
        }
    };

    renderAlert = () => {
        if (this.state.captchaErrorMessage) {
            return (
                <div className={styles.error}>
                    <strong>Oops! </strong> {this.state.captchaErrorMessage}
                </div>
            );
        }
        if (this.props.errorMessage) {
            return (
                <div className={styles.error}>
                    <strong>Oops! </strong> {this.props.errorMessage}
                </div>
            );
        }
    };

    render() {
        const {handleSubmit} = this.props;
        return (
            <div className={styles.margins}>
                <form onSubmit={handleSubmit(this.handleFormSubmit)}>
                    <div className='form-group'>
                        <label htmlFor='email'>Email:</label>
                        <Field
                            name='email'
                            component={renderInput}
                            type='text'
                            validate={[requiredValidator, emailValidator]}
                        />
                    </div>
                    <div className='form-group'>
                        <label htmlFor="password">Password:</label>
                        <Field
                            name='password'
                            component={renderInput}
                            type='password'
                            validate={[requiredValidator, minLengthValidator(6)]}
                        />
                    </div>
                    <div className='form-group'>
                        <label htmlFor="passwordConfirm">Confirm Password</label>
                        <Field
                            name='passwordConfirm'
                            component={renderInput}
                            type='password'
                            validate={[requiredValidator, minLengthValidator(6)]}
                        />
                    </div>
                    {this.renderAlert()}

                    <Recaptcha/>
                    <br/>
                    <button type='submit' className='btn btn-primary'>Sign up</button>
                </form>
            </div>
        );
    }
}

function validate(formProps: { password: string, passwordConfirm: string }) {
    const errors: { password?: string } = {};
    if (formProps.password !== formProps.passwordConfirm) {
        errors.password = 'Passwords must match';
    }

    return errors;
}

function mapStateToProps(state) {
    return {
        errorMessage: state.auth.error,
        isSignUpCaptchaSolved: state.auth.isSignUpCaptchaSolved
    }
}

function mapDispatchToProps(dispatch: Dispatch<any>) {
    return bindActionCreators({signUpUser: signUpUser}, dispatch)
}

const SignUpReduxForm = reduxForm<any>({
    form: 'signup',
    validate
})(SignUp);

export default connect<any>(mapStateToProps, mapDispatchToProps)(SignUpReduxForm)