import * as React from 'react'
import * as ReactRecaptcha from 'react-recaptcha'
import {connect} from 'react-redux'
import {signUpCaptchaSolved} from "../../actions/AuthActions";
import { bindActionCreators, Dispatch } from "redux";
import State from "../../reducers/State";
import Action from "../../actions/Action";

interface RecaptchaProps{
    signUpCaptchaSolved?: () => Action<null>
}

class Recaptcha extends React.Component<RecaptchaProps> {
    render() {
        return (
            <ReactRecaptcha
                sitekey="6LcYmUYUAAAAAE7MIbZ3JtuGHa0Re5BJSa-P6AoB"
                render="explicit"
                verifyCallback={() => {this.props.signUpCaptchaSolved()}}
            />
        );
    }
}
function mapDispatchToProps(dispatch: Dispatch<any>) {
    return bindActionCreators({
        signUpCaptchaSolved: signUpCaptchaSolved
    }, dispatch)
}
export default connect<any>(null,mapDispatchToProps)(Recaptcha);