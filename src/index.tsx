import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import {Router} from 'react-router-dom'
import reduxThunk from 'redux-thunk'

import history from './history'
import App from './components/App';
import reducers from './reducers';
import {AUTH_USER} from "./actions/AuthActions";
import {AUTH_SERVER_TOKEN, FACEBOOK_TOKEN, GOOGLE_TOKEN} from "./global/localStorageKeys";

const createStoreWithMiddleware = applyMiddleware(reduxThunk)(createStore);
const store = createStoreWithMiddleware(reducers);

const token = localStorage.getItem(AUTH_SERVER_TOKEN);
const facebookToken = localStorage.getItem(FACEBOOK_TOKEN);
const googleToken = localStorage.getItem(GOOGLE_TOKEN);

if (token || facebookToken || googleToken) {
    store.dispatch({type: AUTH_USER});
    history.push('app')
}

ReactDOM.render(
    <Provider store={store}>
        <Router history={history}>
            <App />
        </Router>
    </Provider>
    , document.getElementById('root')
);
