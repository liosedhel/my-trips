export const emailValidator = value =>
    value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
        ? 'Invalid email address'
        : undefined;

export const requiredValidator = value => (value ? undefined : 'Required');

export const minLengthValidator = min => value =>
    value && value.length < min ? `Must be ${min} characters or more` : undefined;
